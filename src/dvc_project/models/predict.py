import dvc.api
import fire
import joblib

from dvclive import Live
from src.dvc_project.metrics.metrics import save_metrics


def predict(data_dir: str, model_dir: str, split: str) -> None:
    """Make predictions using a trained machine learning model and save the metrics.

    Args:
        data_dir (str): The directory containing the feature and target files.
        model_dir (str): The directory where the trained model is saved.
        split (str): The data split to use ('train', 'test').
    """

    features = joblib.load(f"{data_dir}/{split}_tfidf_matrix.pkl")
    labels = joblib.load(f"{data_dir}/{split}_target.pkl")

    params = dvc.api.params_show()
    model_path = f"{model_dir}/{params['model']['name']}.pkl"
    model = joblib.load(model_path)

    y_pred_proba = model.predict_proba(features)

    with Live(dir=f"dvclive/{split}", dvcyaml=False) as live:
        save_metrics(live, labels, y_pred_proba, split)


if __name__ == "__main__":
    fire.Fire()
