import dvc.api
import fire
import hydra
import joblib


def train(dataset_path: str, target_path: str, save_dir: str) -> None:
    """Train a machine learning model using the provided dataset and target.

    Args:
        dataset_path (str): The path to the dataset file.
        target_path (str): The path to the target labels file.
        save_dir (str): The directory where the trained model will be saved.
    """
    train_data = joblib.load(dataset_path)
    y_train = joblib.load(target_path)

    params = dvc.api.params_show()

    model = hydra.utils.instantiate(params["model"]["definition"])
    model.fit(train_data, y_train)
    joblib.dump(model, f"{save_dir}/{params['model']['name']}.pkl")


if __name__ == "__main__":
    fire.Fire()
