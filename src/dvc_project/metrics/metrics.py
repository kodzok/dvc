import numpy as np
from sklearn import metrics

from dvclive import Live


def save_metrics(
    live: Live, labels: np.ndarray, y_pred_proba: np.ndarray, split: str
) -> None:
    """Dump all evaluation metrics and plots for given datasets.

    Args:
        live (dvclive.Live): dvclive instance
        labels (np.ndarray): target
        y_pred_proba (np.ndarray): class probabilities of each sample
        split (str): train/test split
    """

    y_pred = y_pred_proba.argmax(axis=1)

    accuracy = metrics.accuracy_score(labels, y_pred)
    precision = metrics.precision_score(labels, y_pred)
    recall = metrics.recall_score(labels, y_pred)
    f1 = metrics.f1_score(labels, y_pred)
    roc_auc = metrics.roc_auc_score(labels, y_pred_proba[:, 1])

    live.log_metric("accuracy", accuracy, plot=False)
    live.log_metric("precision", precision, plot=False)
    live.log_metric("recall", recall, plot=False)
    live.log_metric("f1", f1, plot=False)
    live.log_metric("roc_auc", roc_auc, plot=False)

    live.log_sklearn_plot("roc", labels, y_pred_proba[:, 1], name="roc")
    live.log_sklearn_plot(
        "precision_recall",
        labels,
        y_pred_proba[:, 1],
        name="prc",
        drop_intermediate=True,
    )
    live.log_sklearn_plot("confusion_matrix", labels, y_pred, name="cm")
