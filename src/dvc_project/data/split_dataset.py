import click
import dvc.api
import pandas as pd
from sklearn.model_selection import train_test_split


@click.command()
@click.argument("dataset_path")
def split_dataset(dataset_path: str) -> None:
    """Split dataset into train and test parts.

    Args:
        dataset_path (str): path to dataset
    """
    params = dvc.api.params_show()
    df = pd.read_csv(dataset_path)
    train, test = train_test_split(
        df, test_size=params["test_size"], random_state=params["random_state"]
    )

    train.to_csv("data/processed/train.csv", index=False)
    test.to_csv("data/processed/test.csv", index=False)


if __name__ == "__main__":
    split_dataset()
