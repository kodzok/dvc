import fire
import joblib
import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer


def fit_tfidf(dataset_path: str, save_dir: str) -> None:
    """Train TF-IDF vectorizer.

    Args:
        dataset_path (str): path to dataset
        save_dir (str): directory to save trained vectorizer
    """
    train_data = pd.read_csv(dataset_path)

    vectorizer = TfidfVectorizer()
    vectorizer.fit(train_data["text"])
    vectorizer._stop_words_id = 0

    joblib.dump(vectorizer, f"{save_dir}/tfidf_vectorizer.pkl")


def apply_tfidf(dataset_path: str, vectorizer_path: str, save_path: str) -> None:
    """Transform text data using trained vectorizer.

    Args:
        dataset_path (str): path to dataset
        vectorizer_path (str): path to trained vectorizer
        save_path (str): path to save transformed data
    """

    vectorizer = joblib.load(vectorizer_path)
    data = pd.read_csv(dataset_path)

    tfidf_matrix = round(vectorizer.transform(data["text"]), 5)

    joblib.dump(tfidf_matrix, f"{save_path}_tfidf_matrix.pkl")
    joblib.dump(data["label"], f"{save_path}_target.pkl")


if __name__ == "__main__":
    fire.Fire()
