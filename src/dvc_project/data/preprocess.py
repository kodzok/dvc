import re
import string

import click
import dvc.api
import pandas as pd
from nltk.stem import SnowballStemmer


def clean_text(text):
    # Remove special characters and numbers
    text = re.sub(r"[^A-Za-zÀ-ú ]+", "", text)
    # Analyzing the most used words below, i chose to exclude these because there are too many and are unnecessary
    text = re.sub("book|one", "", text)
    # Convert to lower case
    text = text.lower()
    # remove scores
    text = text.translate(str.maketrans("", "", string.punctuation))
    # Remove extra whitespace
    text = re.sub(r"\s+", " ", text).strip()
    return text


def normalize_text(text):
    stemmer = SnowballStemmer("english")
    normalized_text = []
    for word in text.split():
        stemmed_word = stemmer.stem(word)
        normalized_text.append(stemmed_word)
    return " ".join(normalized_text)


@click.command()
@click.argument("dataset_path")
def preprocess_data(dataset_path: str) -> None:
    """Preprocess and clean text in dataset.

    Args:
        dataset_path (str): path to dataset
    """

    params = dvc.api.params_show()
    df = pd.read_csv(
        dataset_path,
        header=None,
        names=["label", "title", "text"],
        nrows=params["nrows"],
    )
    df["label"] = df.label.map({1: 0, 2: 1})

    df["text"] = df["title"].apply(str) + " " + df["text"].apply(str)
    df.drop(columns=["title"], inplace=True)

    df["text"] = df["text"].apply(clean_text)
    df["text"] = df["text"].apply(normalize_text)

    df.to_csv("data/processed/dataset.csv", index=False)


if __name__ == "__main__":
    preprocess_data()
