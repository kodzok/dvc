FROM python:3.12.3-slim

WORKDIR /app
COPY pyproject.toml pdm.lock ./

RUN pip install pdm && pdm install

ENV PATH="/app/.venv/bin:${PATH}"
